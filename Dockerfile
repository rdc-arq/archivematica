FROM ubuntu:18.04

ENV TZ America/Belem

ENV DEBIAN_FRONTEND noninteractive

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update && apt-get install -y supervisor apt-transport-https \
    unzip wget gnupg  \
    locales rsync \
    && localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8

ENV LANG=en_US.UTF-8

RUN useradd -ms /bin/bash archivematica

RUN wget -O - https://packages.archivematica.org/1.13.x/key.asc | apt-key add -

RUN echo "deb [arch=amd64] http://packages.archivematica.org/1.13.x/ubuntu bionic main" >> /etc/apt/sources.list \
    && echo "deb [arch=amd64] http://packages.archivematica.org/1.13.x/ubuntu-externals bionic main" >> /etc/apt/sources.list

RUN echo "archivematica-mcp-server archivematica-mcp-server/dbconfig-install boolean false" | debconf-set-selections

RUN apt-get update && apt-get -y upgrade

RUN apt-get install -y archivematica-mcp-server=1:1.13.2-1~18.04 \
    && apt-get install -y archivematica-dashboard=1:1.13.2-1~18.04 \
    && apt-get install -y archivematica-mcp-client=1:1.13.2-1~18.04 \
    && rm -rf /var/lib/apt/lists/*

RUN echo "#!/bin/bash" > /usr/share/archivematica/dashboard/migration.sh \
    && echo "set -a -e -x; cat /etc/default/archivematica-dashboard | grep -v 'ARCHIVEMATICA_DASHBOARD_CLIENT\|ARCHIVEMATICA_DASHBOARD_DASHBOARD_ELASTICSEARCH_SERVER' > /tmp/archivematica.var && source /tmp/archivematica.var ; /usr/share/archivematica/virtualenvs/archivematica/bin/python manage.py migrate" >> /usr/share/archivematica/dashboard/migration.sh \
    && chmod +x /usr/share/archivematica/dashboard/migration.sh

WORKDIR /opt/archivematica

RUN chown -R archivematica:archivematica /opt/archivematica

USER archivematica

COPY ./supervisord.conf .

ENTRYPOINT ["/usr/bin/supervisord","-c","/opt/archivematica/supervisord.conf"]
